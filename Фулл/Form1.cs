﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace WindowsFormsApp2
{
    public partial class Form1 : Form
    {
        public double wx = 0, wy = 0;
        public double speed = 2f, zoom = 2f, zoomSpeed = 0.005d;
        public int res = 3;
        private void timer1_Tick_1(object sender, EventArgs e)
        {
            timer1.Stop();
            zoom -= zoomSpeed / zoom;
            Mandelbroad();
        }
        public Form1()
        {
            InitializeComponent();
        }

        private void Form1_Load(object sender, EventArgs e)
        {
            textBox1.Visible = true;
            textBox2.Visible = true;
        }
        public void Mandelbroad()
        {
            
            if (res <= 0)
            {
                res = 1;
            }
            if (comboBox1.Text == "Мандельброт")
                timer1.Start();
            Bitmap frame = new Bitmap(Width / res, Height / res);
            for (int x = 0; x < Width / res; x++)
            {
                for (int y = 0; y < Height / res; y++)
                {
                    double a = (double)((x + ( res / zoom)) - ((Width / 2d) / res)) / (double)(Width / zoom / res / 1.777f);
                    double b = (double)((y + ( res / zoom)) - ((Height / 2d) / res)) / (double)(Height / zoom / res);

                    Numbers c = new Numbers(a, b);
                    Numbers z = new Numbers(0, 0);


                    int it = 0;

                    do
                    {
                        it++;
                        z.Sqr();
                        z.Add(c);
                        if (z.Magn() > 2.0d)
                        {
                            break;
                        }
                    } while (it < 100);


                    frame.SetPixel(x, y, Color.FromArgb((byte)(it * 2.55f), (byte)(it * 2.55f), (byte)(it * 2.55f)));
                }
            }

            pictureBox1.Image = frame;
            pictureBox1.SizeMode = PictureBoxSizeMode.StretchImage;
        }
        Point dot; bool ok;
        
        private void button1_Click_1(object sender, EventArgs e)
        {

            if (comboBox1.Text == "Дерево")
            {
                pictureBox1.Refresh();
                small_three(pictureBox1.Width / 2, 0, 0, 150);
                comboBox1.Visible = false;
            }
            if (comboBox1.Text == "Квадратне дерево")
            {
                pictureBox1.Refresh();
                Three(280, 460, 100, 0);
                comboBox1.Visible = false;
            }
            if (comboBox1.Text == "Трикутник(Хаос)")
            {
                pictureBox1.Refresh();
                int x, y;
             
                    ok = int.TryParse(textBox1.Text, out x);
                    if (!ok)
                    {
                        MessageBox.Show("Помилка введення значення x!", "Помилка", MessageBoxButtons.OK, MessageBoxIcon.Error);
                        textBox1.Text = "";
                        x = 0;
                    }
               
               
                ok = int.TryParse(textBox1.Text, out y);
                if (!ok)
                {
                    MessageBox.Show("Помилка введення значення y!", "Помилка", MessageBoxButtons.OK, MessageBoxIcon.Error);
                    textBox1.Text = "";
                    y = 0;
                }
              
                dot.X = 100;
                dot.Y = 100;
                dot.X = x;
                dot.Y = y;
                Tserpynski_chaos(dot);
                comboBox1.Visible = false;
            }
            if (comboBox1.Text == "Мандельброт")
            {
                pictureBox1.Refresh();
                
                Mandelbroad();
                comboBox1.Visible = false;
            }
            if (comboBox1.Text == "Сніжинка Коха")
            {
                pictureBox1.Refresh();

                Draw_KOCH();
                comboBox1.Visible = false;
            }
            if (comboBox1.Text=="Трикутник(звич)")
            {
                PointF[] points = new PointF[3];
                points[0].X = (int)(Width / 1.1);
                points[0].Y = (int)(Height / 1.1);
                points[1].X = (int)(Width / 2);
                points[1].Y = 0;
                points[2].X = 10;
                points[2].Y = (int)(Height / 1.1);
                var bt = pictureBox1.CreateGraphics();
                DrawTriangle(bt, 8, points[1], points[2], points[0]);
            }
        }

        public void Tserpynski_chaos(Point dot)
        {
            var bmp = new Bitmap(Width, Height);
            

            Random rnd = new Random();
            Point[] points = new Point[1000000];
            textBox1.Visible = false;
            textBox2.Visible = false;
            points[0].X = 0;
            points[0].Y = 0;
            points[1].X = (int)(Width / 1.1);
            points[1].Y = 0;
            points[2].X = (int)(Width / 2);
            points[2].Y = (int)(Height / 1.1);
          
            points[3].X = dot.X;
            points[3].Y = dot.Y;
            for (int i = -3; i < 3; i++)
            {
                for (int j = -3; j < 3; j++)
                {
                    if (points[3].X + i < Width && points[3].Y + j < Height)
                    {
                        bmp.SetPixel(points[3].X + i, points[3].Y + j, Color.Red);
                    }
                }
            }
            int a;
            for (int i = 4; i < 1000000; i++)
            {
                a = rnd.Next(3);

                points[i].X = (int)((points[a].X + points[i - 1].X) / 2);
                points[i].Y = (int)((points[a].Y + points[i - 1].Y) / 2);
                if (points[i].X < Width && points[i].Y < Height)
                {
                    bmp.SetPixel(points[i].X, points[i].Y, Color.Black);
                }
            }


            pictureBox1.Image = bmp;
            pictureBox1.SizeMode = PictureBoxSizeMode.StretchImage;
        }
        public void Rect(double x1, double y1, double l, double a1)
        {
            Graphics g = pictureBox1.CreateGraphics();
            Random r = new Random();
            g.SmoothingMode = System.Drawing.Drawing2D.SmoothingMode.HighQuality;
            g.CompositingQuality = System.Drawing.Drawing2D.CompositingQuality.HighQuality;
            System.Drawing.Brush brush = new System.Drawing.SolidBrush(Color.FromArgb(r.Next(0, 251), r.Next(0, 251), r.Next(0, 251)));
            Pen myPen = new Pen(brush, 2);

            double x2 = Math.Round(x1 + (l * Math.Cos(a1)));
            double y2 = Math.Round(y1 - (l * Math.Sin(a1)));

            double x3 = Math.Round(x1 + (l * Math.Sqrt(2) * Math.Cos(a1 + Math.PI / 4)));
            double y3 = Math.Round(y1 - (l * Math.Sqrt(2) * Math.Sin(a1 + Math.PI / 4)));

            double x4 = Math.Round(x1 + (l * Math.Cos(a1 + Math.PI / 2)));
            double y4 = Math.Round(y1 - (l * Math.Sin(a1 + Math.PI / 2)));

            g.DrawLine(myPen, (int)x1, (int)y1, (int)x2, (int)y2);
            g.DrawLine(myPen, (int)x2, (int)y2, (int)x3, (int)y3);
            g.DrawLine(myPen, (int)x3, (int)y3, (int)x4, (int)y4);
            g.DrawLine(myPen, (int)x4, (int)y4, (int)x1, (int)y1);

        }

        public void Three(double x, double y, double l, double a)
        {
            
            if (l > 8)
            {
                Rect(x, y, l, a);

                Three(
                    x - l * Math.Sin(a),
                    y - l * Math.Cos(a),
                    l / Math.Sqrt(2),
                    a + Math.PI / 4
                );

                Three(
                    x - l * Math.Sin(a) + l / Math.Sqrt(2) * Math.Cos(a + Math.PI / 4),
                    y - l * Math.Cos(a) - l / Math.Sqrt(2) * Math.Sin(a + Math.PI / 4),
                    l / Math.Sqrt(2),
                    a - Math.PI / 4);

            }
        }
        public void small_three(int x, int y, int angle, int length)
        {
            Graphics g = pictureBox1.CreateGraphics();
            Random rnd = new Random();
            double x1, y1;
            System.Drawing.Brush brush = new System.Drawing.SolidBrush(Color.FromArgb(rnd.Next(0, 251), rnd.Next(0, 251), rnd.Next(0, 251)));
            Pen myPen = new Pen(brush, 2);
            x1 = x + length * Math.Sin(angle * Math.PI / 360.0);
            y1 = y + length * Math.Cos(angle * Math.PI / 360.0);
            g.DrawLine(myPen, x, pictureBox1.Height - y, (int)x1, pictureBox1.Height - (int)y1);

            if (length > 2)
            {
                small_three((int)x1, (int)y1, angle + 40, (int)(length / 1.5));
                small_three((int)x1, (int)y1, angle - 40, (int)(length / 1.5));


            }

        }
        static Pen pen_koch1; 
        static Graphics koch_g;

        private void pictureBox1_Click(object sender, EventArgs e)
        {
            if (comboBox1.Text == "Трикутник(Хаос)")
            {
                MessageBox.Show("Введіть будь ласка координати точки у поля");
                textBox1.Visible = true;
                textBox2.Visible = true;
            }
        }

        private void button2_Click(object sender, EventArgs e)
        {
            comboBox1.Visible = true;
            Bitmap btm = new Bitmap(Width, Height);
            var pen = new Pen(Color.White);
            var Brush = new SolidBrush(Color.Red);
            var g = pictureBox1.CreateGraphics();
            for (int i = 0; i < Width; i++)
                for (int j = 0; j < Height; j++)
                {
                    btm.SetPixel(i,j,Color.White);
                
                }
            pictureBox1.Image = btm;
            timer1.Stop();
        }

      
        static Pen pen_koch2;

        private void button3_Click(object sender, EventArgs e)
        {
            Application.Exit();
        }

        public void Draw_KOCH()
        {
            //Выбираем цвета зарисовки 
            pen_koch1 = new Pen(Color.Green, 1);
            pen_koch2 = new Pen(Color.Blue, 1);
            //Объявляем объект "g" класса Graphics
            koch_g = pictureBox1.CreateGraphics();

            koch_g.Clear(Color.Black);//Зарисовка экрана черным фоном

            //Определим коорднаты исходного треугольника
            var point1 = new PointF(200, 200);
            var point2 = new PointF(500, 200);
            var point3 = new PointF(350, 400);

            //Зарисуем треугольник
            koch_g.DrawLine(pen_koch1, point1, point2);
            koch_g.DrawLine(pen_koch1, point2, point3);
            koch_g.DrawLine(pen_koch1, point3, point1);

            //Вызываем функцию Fractal для того, чтобы
            //нарисовать три кривых Коха на сторонах треугольника
            Fractal(point1, point2, point3, 5);
            Fractal(point2, point3, point1, 5);
            Fractal(point3, point1, point2, 5);

        }


        public int Fractal(PointF p1, PointF p2, PointF p3, int iter)
        {
            if (iter > 0)  
            {
              
                var p4 = new PointF((p2.X + 2 * p1.X) / 3, (p2.Y + 2 * p1.Y) / 3);
                var p5 = new PointF((2 * p2.X + p1.X) / 3, (p1.Y + 2 * p2.Y) / 3);
                var ps = new PointF((p2.X + p1.X) / 2, (p2.Y + p1.Y) / 2);
                var pn = new PointF((4 * ps.X - p3.X) / 3, (4 * ps.Y - p3.Y) / 3);
                koch_g.DrawLine(pen_koch1, p4, pn);
                koch_g.DrawLine(pen_koch1, p5, pn);
                koch_g.DrawLine(pen_koch2, p4, p5);
                //recursia
                Fractal(p4, pn, p5, iter - 1);
                Fractal(pn, p5, p4, iter - 1);
                Fractal(p1, p4, new PointF((2 * p1.X + p3.X) / 3, (2 * p1.Y + p3.Y) / 3), iter - 1);
                Fractal(p5, p2, new PointF((2 * p2.X + p3.X) / 3, (2 * p2.Y + p3.Y) / 3), iter - 1);

            }
            return iter;
        }
        private void DrawTriangle(Graphics gr, int level, PointF top_point, PointF left_point, PointF right_point)
        {

            if (level == 0)
            {

                PointF[] points =
                {
            top_point, right_point, left_point
        };
                gr.FillPolygon(Brushes.Red, points);
            }
            else
            {
                //точки трик
                PointF left_mid = new PointF(
                    (top_point.X + left_point.X) / 2f,
                    (top_point.Y + left_point.Y) / 2f);
                PointF right_mid = new PointF(
                    (top_point.X + right_point.X) / 2f,
                    (top_point.Y + right_point.Y) / 2f);
                PointF bottom_mid = new PointF(
                    (left_point.X + right_point.X) / 2f,
                    (left_point.Y + right_point.Y) / 2f);

                // Рекурсія на малі трикутники
                DrawTriangle(gr, level - 1,
                    top_point, left_mid, right_mid);
                DrawTriangle(gr, level - 1,
                    left_mid, left_point, bottom_mid);
                DrawTriangle(gr, level - 1,
                    right_mid, bottom_mid, right_point);
                pictureBox1.SizeMode = PictureBoxSizeMode.StretchImage;
            }
        }

    }
   




}
